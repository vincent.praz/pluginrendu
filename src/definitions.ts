declare module '@capacitor/core' {
  interface PluginRegistry {
    PluginRendu: PluginRenduPlugin;
  }
}

export interface PluginRenduPlugin {
  echo(options: { value: string }): Promise<{ value: string }>;
  getContacts(filter: string): Promise<{results: any[]}>; // TODO:
}
