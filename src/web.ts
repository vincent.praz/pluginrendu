import { WebPlugin } from '@capacitor/core';
import { PluginRenduPlugin } from './definitions';

export class PluginRenduWeb extends WebPlugin implements PluginRenduPlugin {
  constructor() {
    super({
      name: 'PluginRendu',
      platforms: ['web'],
    });
  }

  async echo(options: { value: string }): Promise<{ value: string }> {
    console.log('ECHO', options);
    return options;
  }

  // TODO:
  async getContacts(filter: string): Promise<{ results: any[] }> {
    console.log('filter: ', filter);
    return {
      results: [{
        firstName: 'Dummy',
        lastName: 'Entry',
        telephone: '123456'
      }]
    };
  }


}

const PluginRendu = new PluginRenduWeb();

export { PluginRendu };

import { registerWebPlugin } from '@capacitor/core';
registerWebPlugin(PluginRendu);
